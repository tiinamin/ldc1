#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 12:23:29 2021

@author: tiinamin

For plotting SGWB models, instrument functions, and different data sets.

#FIXME TDI variable selection automatic; however, need a loop or something if wanting to plot many vars in same plot?

"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import data
import lisa

var = 'X'
F = 25 # reference frequency f*
seglength = '1024' # length of each segment to be windowed (see rows 38-44: different window size data saved in different files so only those options you've saved are available)
Sa_est = 10** -29.0471
Sp_est = 10** -21.6514
A_est = 10** -8.8792
alpha_est = 0.5571
f_high = 1e-2 # high freq limit for the fit
f_low= 1e-5 # low freq limit for the fit

response = data.instrument(var, 'instrnoise')[1] # response function same for X, Y, Z
noise = data.instrument(var, 'instrnoise')[0] # noise same for X, Y, Z

def powerlaw(f, A, alpha):
    """Defines the expected signal omega_gw = A * (f/F)**alpha * R(f)"""
    h = 0.671
    H = h * 100 * 3.24e-20
    pl_PSD = (f/F)**alpha * A * ((3 * H**2) / (4 * np.pi**2 * f**3)) # powerlaw conversion from energy density (omega) to PSD
    return pl_PSD * response(f) * 0.5

def model(f, A, alpha, Sa, Sp):
    """Model for the fit, including noise if present in data"""
    return powerlaw(f, A, alpha) + noise(f, N_acc=Sa, N_pos=Sp)

"""Data"""
# nless_data = data.psd('noiseless', seglength, var)
TDIdata = data.psd('instrnoise', seglength, var)
# nrand_data = data.psd('randnoise', seglength, var)

f = data.freq('instrnoise', seglength, var) # frequencies are the same for all data with same seglength

"""Cross-correlated noise"""
# csd_noise_XY = lisa.N_XYcross(f)
# csd_noise_XY_scipy = lisa.N_XYcross_scipy(f)
# csd_noise_AE = lisa.N_AEcross(f)

"""Expected models"""
sgwb_noise = model(f, 3.55e-9, 0.667, lisa.acc, lisa.oms) # "correct answer" for model including noise if present in data
# sgwb = powerlaw(f, 3.55e-9, 0.667) # "correct answer" for the powerlaw signal

"""Results"""
sgwb_noise_est = model(f, A_est, alpha_est, Sa_est, Sp_est)
# sgwb_est = powerlaw(f, A_est, alpha_est)

"""Relative differences"""
# R_data_diff = (nless_data - sgwb) / sgwb # rel. difference between noiseless data & powerlaw * response
# R_data_diff_noise = (TDIdata - sgwb_noise) / sgwb_noise # rel. difference between data & model = (powerlaw * response + noise)
# nrand_diff = (nrand_data - TDIdata) / (0.5 * (nrand_data + TDIdata)) # rel. difference between instrnoise & noiserand datasets
# noise_diff = (TDIdata - noise(f)) / (0.5 * (noise(f) + TDIdata)) # rel. difference between data and noise function
# noise_inj_est_diff = (noise(f, Sa_est, Sp_est) - noise(f)) / (0.5 * (noise(f, Sa_est, Sp_est) + noise(f))) # rel. difference between true & est. noise
model_inj_est_diff = (sgwb_noise_est - sgwb_noise) / (0.5 * (sgwb_noise_est + sgwb_noise)) # rel. difference between true & est. model

#%%
"""Plot"""
fig = plt.figure(figsize=(12,9))
# fig.suptitle(r"$\mathcal{R}$ & data relative difference")#"using $\mathcal{R}_{exact} \times 16sin^2(\frac{f}{f_*})(\frac{f}{f_*})^2$")
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 2])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
# fig, ax0 = plt.subplots(1, 1, figsize=(12,7))
# fig.suptitle("Noise fit results | seglength {0:s} | f-range [{1:.1e}, {2:.1e}]".format(seglength, f_low, f_high))


"""Functions"""
# ax0.loglog(f, noise(f), '--', label=r"Injected noise: $S_a=$ {0:.0e}, $S_p=$ {1:.2e}".format(lisa.acc, lisa.oms), color='steelblue')
# ax0.loglog(f, sgwb, '--', label=r"Injected SGWB: $\Omega=3.55e-9, \alpha=0.667$", color='sandybrown')
ax0.loglog(f, sgwb_noise, '--', label="Injected SGWB with instrument noise", color='silver')
# ax0.loglog(f, csd_noise_XY, label='LDC manual function for XY cross-correlation')
# ax0.loglog(csd_noise_XY_scipy[0], csd_noise_XY_scipy[1], label='scipy.signal.csd computation of XY cross-correlation')
# ax0.loglog(csd_noise_AE[0], csd_noise_AE[1], label='AE cross-correlation')

"""MCMC results"""
# ax0.loglog(f, noise(f, Sa_est, Sp_est), label=r"Estimated noise: $S_a=$ {0:.2e}, $S_p=$ {1:.2e}".format(Sa_est, Sp_est), color='navy')
# ax0.loglog(f, sgwb_est, label=r"Estimated SGWB: $\Omega=$ {0:.2e}, $\alpha=$ {1:.3f}".format(A_est, alpha_est), color='darkorange')
ax0.loglog(f, sgwb_noise_est, label=r"MCMC estimate of SGWB with noise")


"""Noiseless data"""
# ax0.loglog(f, nless_data, label="Noiseless data", color='darkorange')


"""Data with random noise"""
# ax0.loglog(f, nrand_data, label="Data, random noise", color='mediumvioletred')


"""Data with instrument noise"""
# ax0.loglog(f, TDIdata, 'x', label="TDI data, instrument noise", color='slateblue')


"""Relative differences"""
# ax1.semilogx(f, R_data_diff_noise, label="Relative difference between data & model", color='grey')
# ax1.semilogx(f, R_data_diff, label="Relative difference between data & model", color='grey')
# ax1.semilogx(f, nrand_diff, label="Relative difference between random & instrument noise data sets", color='grey')
# ax1.semilogx(f, noise_diff, label="Relative difference between T channel data and T channel noise", color='grey')
# ax1.semilogx(f, noise_inj_est_diff, label="Noise relative difference", color='steelblue')
ax1.semilogx(f, model_inj_est_diff, label="SGWB relative difference", color='sandybrown')

"""Plot specs"""
ax0.grid(True, axis='both', alpha=0.5)
ax0.set_xlabel("Frequency [Hz]")
ax0.set_ylabel(r"PSD [Hz$^{-1}$]")
# ax0.set_ylim(1e-43, 1e-37)
ax0.legend(loc='lower left')

ax1.grid(True, axis='both', alpha=0.5)
ax1.set_xlabel("Frequency [Hz]")
# ax1.set_ylabel(r"PSD [Hz$^{-1}$]")
# ax1.set_ylim(-0.1, 1)
ax1.legend(loc='lower left')

plt.tight_layout()

# plt.savefig("Comparisons/Noisefit_results_128x1024_frange2a.pdf")
