#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 13 15:27:17 2019

@author: tiinamin

For
-plotting a desired chunk of data using scipy.signal.fft or scipy.signal.welch
-comparing these methods
-plotting the denoised signal
-creating .npz files of the data for e.g. curve fitting
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import h5py
from scipy.fft import fft
import scipy.signal as sig
import lisa

# fh5 = h5py.File('LDC1-6_SGWB_v1.hdf5','r')
# fh5 = h5py.File('LDC1-6_SGWB_v1_noiseless.hdf5','r')
fh5 = h5py.File('LDC1-6_SGWB_v1_noiserand.hdf5','r')
d = fh5['/H5LISA/PreProcess/TDIdata']
# print("Shape: ", d.shape)
# print("Datatype: ", d.dtype)
# print("Key:")
# for key in fh5.keys():
#     print(key)
# print("Groups: ")
# group = fh5[key]
# for key in group.keys():
#     print(key)
# preprocess = fh5['/H5LISA/PreProcess']
# print("Subgroups in PreProcess:")
# for key in preprocess.keys():
#     print(key)
# simulation = fh5['/H5LISA/Simulation']
# print("Subgroups in Simulation:")
# for key in simulation.keys():
#     print(key)
# user = fh5['/H5LISA/UserRequest']
# print("Subgroups in UserRequest:")
# for key in user.keys():
#     print(key)
# configparams = fh5['H5LISA/UserRequest/ConfigParams']
# print("Subgroups in ConfigParams:")
# for key in configparams.keys():
#     print(key)
# request = fh5['H5LISA/UserRequest/ConfigParams/Request']
# print(request[()])
# noises = fh5['/H5LISA/Simulation/InterpolationNoises']
# print(noises[()])


pos = 0
# chunk = 64*1024
chunk = 12623041 # full length of LDC1-6 data sets
seglength = chunk-1 # lenght of data segment to be multiplied by a window function in Welch transform
overlap = 0.0 * seglength # window overlap for Welch transform

t = d[pos:pos+chunk-1,0]
X = d[pos:pos+chunk-1,1]
Y = d[pos:pos+chunk-1,2]
Z = d[pos:pos+chunk-1,3]
A, E, T = lisa.AET(X,Y,Z)

P = d[pos+chunk-1,0] - d[pos,0] # observation period

dt = t[1]- t[0] # time step
N_windows = chunk/seglength # number of windows

# f = np.logspace(-6, -1, num=500, base=10)
f = np.linspace(0, 0.5/dt, chunk//2)

"""Welch PSD's for the 6 TDI channels"""
Wx = sig.welch(X, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap) # using rect window & seglength=chunk-1 is equivalent to fft'ing
# Wy = sig.welch(Y, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap)
# Wz = sig.welch(Z, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap)
# Wa = sig.welch(A, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap)
# We = sig.welch(E, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap)
# Wt = sig.welch(T, 1/dt, nperseg=seglength)#, window="bartlett", noverlap=overlap)

# W_norm = chunk / 10
# W_norm = 1
# S_norm = 1
# Wx_norm = W_norm*Wx[1]
# Wy_norm = W_norm*Wy[1]
# Wz_norm = W_norm*Wz[1]
# Wa_norm = W_norm*Wa[1]
# We_norm = W_norm*We[1]
# Wt_norm = W_norm*Wt[1]

# Wx = (Wx[0], Wx_norm)
# Wy = (Wy[0], Wy_norm)
# Wz = (Wz[0], Wz_norm)
# Wa = (Wa[0], Wa_norm)
# We = (We[0], We_norm)
# Wt = (Wt[0], Wt_norm)

"""Denoised Welch PSD's"""
# X_dn = Wx[1] - lisa.N_XYZ(Wx[0])
# Y_dn = Wy[1] - lisa.N_XYZ(Wy[0])
# Z_dn = Wz[1] - lisa.N_XYZ(Wz[0])
# A_dn = Wa[1] - lisa.N_AE(Wa[0])
# E_dn = We[1] - lisa.N_AE(We[0])
# T_dn = Wt[1] - lisa.N_T(Wt[0])

"""Save transformed data into .npz file"""
# np.savez("welch_SGWB_w128x1024bart.npz", X=Wx, Y=Wy, Z=Wz, A=Wa, E=We, T=Wt, P=P, N=N_windows)

plt.figure(figsize=(12,7))

"""FFT'd signals"""
# plt.subplot(1,2,1)
# plt.loglog(f, lisa.S(X, chunk), label="X fft", zorder=2)
# plt.loglog(f, lisa.S(Y, chunk), label="Y fft")
# plt.loglog(f, lisa.S(Z, chunk), label="Z fft")
# plt.legend(loc='lower left')
# plt.xlabel("Hz")
# plt.ylabel(r"$\delta f/f$")
# plt.ylim(1e-60,1e-32)
# plt.xlim(9e-3,1.1e-1)
# plt.grid(axis='both', linestyle=':')

# plt.subplot(2,2,2)
# plt.loglog(f, lisa.S(A, chunk), label="A fft")
# plt.loglog(f, lisa.S(E, chunk), label="E fft")
# plt.loglog(f, lisa.S(T, chunk), label="T fft")
# plt.legend(loc='lower left')
# plt.xlabel("Hz")
# plt.ylabel(r"$\delta f/f$")
# plt.ylim(1e-60,1e-32)
# plt.grid(axis='both', linestyle=':')

"""FFT'd and windowed signals""" # FIXME how to divide signal into overlapping segments?
# w = sig.windows.hann(seglength)
# sig_fft_win = abs(fft(X*w, norm="ortho")[0:seglength//2])**2

"""Denoised Welch'd signals"""
# plt.subplot(2,2,1)
# plt.loglog(Wx[0], X_dn, label = "X denoised")
# plt.loglog(Wy[0], Y_dn, label = "Y denoised")
# plt.loglog(Wz[0], Z_dn, label = "Z denoised")
# plt.legend(loc='lower left')
# plt.xlabel("Hz")
# plt.ylabel(r"Hz$^{-1}$")

# plt.subplot(2,2,2)
# plt.loglog(Wa[0], A_dn, label = "A denoised")
# plt.loglog(Wy[0], E_dn, label = "E denoised")
# plt.loglog(Wt[0], T_dn, label = "T denoised")
# plt.legend(loc='lower left')
# plt.xlabel("Hz")
# plt.ylabel(r"Hz$^{-1}$")

"""Welch'd signals"""
plt.subplot(1,1,1)
plt.loglog(Wx[0], Wx[1], label = "PSD from Welch method", zorder=1)
# plt.loglog(Wy[0], Wy[1], label = "Y welch")
# plt.loglog(Wz[0], Wz[1], label = "Z welch")
plt.legend(loc='lower left')
plt.xlabel("f [Hz]")
plt.ylabel(r"PSD [Hz$^{-1}]$")
# plt.ylim(1e-49,1e-37)
# plt.xlim(5e-2,1.1e-1)
# plt.grid(True, axis='both', alpha=0.5)
# plt.tight_layout()

# plt.subplot(2,2,4)
# plt.loglog(Wa[0], We[1], label = "A welch")
# plt.loglog(Wy[0], Wy[1], label = "E welch")
# plt.loglog(Wt[0], Wt[1], label = "T welch")
# plt.legend(loc='lower left')
# plt.xlabel("Hz")
# plt.ylabel(r"Hz$^{-1}$")
# plt.ylim(1e-60,1e-32)
# plt.grid(axis='both', linestyle=':')

"""t-domain data (= plain data with no F-transform)"""
# plt.subplot(2,1,1)
# plt.plot(t, X, label="Time-domain data")
# plt.plot(t, Y, label="Y data")
# plt.plot(t, Z, label="Z data")
# plt.legend(loc='lower left')
# plt.xlabel("t [5s]")
# plt.ylabel(r"$\Delta f / f$")
# plt.ylim(-1.6e-19, 1.6e-19)

# plt.subplot(1,2,2)
# plt.plot(t, T, label="T data")
# plt.plot(t, A, label="A data")
# plt.plot(t, E, label="E data")
# plt.legend(loc='lower left')
# plt.xlabel("t (5 s)")
# plt.ylabel(r"$\Delta f / f$")
# plt.ylim(-1.6e-19, 1.6e-19)

"""Noise and response"""
# plt.loglog(Wx[0], lisa.N_XYZ(Wx[0]), label="X noise")

# # f = Wx[0][1:]
# # RX_fit = lisa.R_XYZ(f)
# RX_exact = lisa.R_XYZ_exact(f)
# # diff = RX_fit-RX_exact
# # diff_rel = (RX_fit-RX_exact) / (0.5 * (RX_exact+RX_fit))
# gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1])
# ax0 = plt.subplot(gs[0])
# ax0.loglog(f, RX_exact, label="R_exact", color='grey')
# # ax0.loglog(f, RX_fit, label="R_fit", color='gold')
# ax0.legend(loc='upper left')
# ax0.grid(True, axis='both', alpha=0.5)

# ax1 = plt.subplot(gs[1])
# # ax1.semilogx(f, abs(diff_rel), label="Relative difference between response functions", color='steelblue')
# ax1.legend(loc='lower left')
# ax1.grid(True, axis='both', alpha=0.5)
# # ax1.set_xlim(1e-5, 1e-2)
# # ax1.set_ylim(-0.02, 0.01)
# plt.tight_layout()

# plt.savefig("time_vs_frequency.pdf")