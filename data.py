#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 09:05:15 2021

@author: tiinamin
"""

import numpy as np
import lisa
from gw_data_tools import lisa_noise_and_response as lnr


def instrument(var, dataset):
    """Choose noise and response functions from lisa.py
    for a certain TDI variable and data set (noiseless etc.)
    """
    if var=='X' or var=='Y' or var=='Z':
        noise = lisa.N_XYZ
        response = lisa.R_combination
        col = 0 # mainly for legend automation
    elif var=='A':
        noise = lisa.N_AE
        response = lisa.R_AE
        col = 1
    elif var=='E':
        noise = lisa.N_AE
        response = lisa.R_AE
        col = 1
    elif var=='T':
        noise = lisa.N_T
        response = lisa.R_T
        col = 2
    elif var == 'AE':
        noise = lisa.N_AEcross
        response == 0
        col == None
    if dataset == 'noiseless':
        noise = lambda f: f*0.0
    return noise, response, col

def data(dataset, seglength):
    """Load data set of a certain window size."""
    if dataset == 'noiseless':  # welch'd PDS of the noiseless data set
        data = np.load('welch_SGWB_nless_w' + seglength + '.npz')
    elif dataset == 'randnoise': # welch'd PDS of the random noise level data set
        data = np.load('welch_SGWB_nrand_w' + seglength + '.npz')
    elif dataset == 'instrnoise': # welch'd PDS of the known instrumental noise level data set
        data = np.load('welch_SGWB_w' + seglength + '.npz')
    elif dataset == 'mydata': # chi2 distributed data with LDC parameters for checking if codes work
        data = np.load('myX2data_w' + seglength + '.npz') 
    elif dataset == 'csd': # cross-correlated data of A and E channels
        data = np.load('csd_SGWB_w' + seglength + '.npz')
    return data

def freq(dataset, seglength, var):
    """Return frequency array from loaded data set."""
    f = data(dataset, seglength)[var][0][1:]
    return f

def psd(dataset, seglength, var):
    """Return PSD of the loaded data set."""
    psd = data(dataset, seglength)[var][1][1:]
    return psd

def period(dataset, seglength):
    """Return observation period."""
    return data(dataset, seglength)['P'].item(0)

def N_windows(dataset, seglength):
    """Return number of windows used in welch/csd."""
    return data(dataset, seglength)['N'].item(0)