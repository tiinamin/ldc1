Aim and objective
-----------------

Recovering a stochastic gravitational wave background (SGWB) signal from the first LISA Data Challenge data. Studying and comparing different methods of extracting a SGWB signal from a space-borne gravitational wave observatory.

Methods tested
--------------

1. Least-squares fit, recovering SGWB parameters only
2. Markov Chain Monte Carlo, recovering both SGWB and instrument noise parameters

Contents
--------

`project_documentation.pdf` - a full history and documentation of the project and results

`Chisquare_curvefit_PSD.py` - code for least-squares fitting

`SGWB_MCMC_noisefit.py` - code for the Markov Chain Monte Carlo using Cobaya (Code for Bayesian Analysis in Cosmology, https://cobaya.readthedocs.io/en/latest/index.html)

`lisa.py` - module containing instrument noise and response functions used in both of the above

`data.py` - module for reading the data files under Data

`Data` - .npz data files containing Welch-transformed data generated from LDC1-6 (SGWB) data, available for download at https://lisa-ldc.lal.in2p3.fr/challenge1

`play_PSD.py` - code for creating, plotting and saving said Welch-transformed data

`play_functions.py` - code for plotting results against predictions

* Known instrument noises / Noiseless (nless) / Random instrument noises (nrand)

* Windowed data with window sizes (w) from n=512 to n=1024*1024