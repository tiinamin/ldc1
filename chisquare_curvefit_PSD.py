#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 13:00:26 2020

@author: tiinamin

Fit to the power spectral density of the LISA data, to find the powerlaw amplitude and spectral index (alpha)
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from scipy import optimize, integrate
import data


var = 'X' # TDI channel: X, Y, Z (Michelsons), or A, E, T (orthogonal, noise-uncorrelated channels)
F = 25 # reference frequency f*
F_best = 25
chunk = 12623041
dataset = 'noiseless' # 'noiseless', 'randnoise' or 'instrnoise' OR now also available a limited edition of 'mydata'! only with 'X' and seglength=1024
seglength = '1024' # length of each segment to be windowed (see rows 38-44: different window size data saved in different files so only those options you've saved are available)
f_high = 2.5e-2 # high freq limit for the fit
f_low= 4e-4 # low freq limit for the fit


"""Initial guesses for fit parameters"""
A_init = 3.55e-9 # initial guess for the powerlaw(f, A, alpha) amplitude A [energy density = omega]
alpha_init = 2/3 # initial guess for the powerlaw(f, A, alpha) spectral index alpha


"""Fit functions"""
noise, response, col = data.instrument(var, dataset) # LISA noise & response functions; col is for plot legend automation

def powerlaw(f, A, alpha):
    """Defines the expected signal omega_gw = A * (f/F)**alpha * R(f)"""
    h = 0.671
    H = h * 100 * 3.24e-20
    pl_PSD = (f/F)**alpha * A * ((3 * H**2) / (4 * np.pi**2 * f**3)) # powerlaw conversion from energy density (omega) to PSD
    return pl_PSD * response(f) * 0.5

def model(f, A, alpha):
    """Model for the fit, including noise if present in data"""
    return powerlaw(f, A, alpha) + noise(f)

def SNRfunc(f, A, alpha):
    """Calculate signal-to-noise ratio"""
    return powerlaw(f, A, alpha)**2 / noise(f)**2


"""Data to fit to"""
all_f = data.freq(dataset, seglength, var) # entire frequency range [1e-6, 1e-1]
all_psd = data.psd(dataset, seglength, var) # PSD's for the entire frequency range
included = (all_f > f_low) & (all_f < f_high)
f = all_f[included] # frequency range for the fit
psd = all_psd[included] # data to be fitted to
P = data.period(dataset, seglength) # duration of observation

stdev = np.sqrt(noise(f)**2 + powerlaw(f, A_init, alpha_init)**2) # standard deviation for PSD; include powerlaw for the noiseless data error
p0 = [A_init, alpha_init]
try:
    popt, pcov = optimize.curve_fit(model, f, psd, p0, sigma=stdev, absolute_sigma=True) # exclude sigma & absolute_sigma if fitting to noiseless
except RuntimeError as e:
    print('Problem fitting: ', e)
    popt = [0,0]
    pcov = np.zeros((2,2))


"""Fit"""
# popt[0] *= (F_best/F)**(2/3) # amplitude corresponding to chosen F
# pcov[0,0] = A_init * (F/25)**(2/3) # amplitude variance corresponding to chosen F
amp = popt[0] # fit estimate for amplitude in omega
a = popt[1] # fit estimate for alpha
fit = model(all_f, amp, a) # f for plotting only fit range, all_f for plotting over all frequencies
gw_signal = powerlaw(all_f, amp, a) # f for plotting only fit range, all_f for plotting over all frequencies

mod = model(all_f, 3.55e-9, 0.667) # "correct answer" for fit model
gw_theoretical = powerlaw(all_f, 3.55e-9, 0.667) # "correct answer" for the powerlaw signal

N = data.N_windows(dataset, seglength)
stdev_res = np.sqrt(noise(all_f)**2 + powerlaw(all_f, A_init, alpha_init)**2)
residuals = (fit - all_psd) / (mod / np.sqrt(N))

chisq = sum(((psd - model(f, amp, a)) / stdev)**2) # chi-squared statistic for the fit
SNR2 = int(P) * integrate.quad(SNRfunc, f_low, f_high, args=(amp, a))[0] # signal-to-noise squared
SNR = np.sqrt(SNR2) # signal-to-noise ratio


"""Print fit results"""
np.set_printoptions(precision=3)
print(var, '; F = {:.1e}'.format(F))
chi = '{:.2f}'.format(chisq) if chisq > 0.01 else '{:.2e}'.format(chisq)
print('Chi-squared', chi)
print('Degrees of freedom', len(psd)-1)
# print('Reduced chi-squared', '{:.4f}'.format(chisq/(len(psd)-1)))
print('A, alpha')
# popt[0] *= ((3 * (67.1*3.24e-20)**2) / (4 * np.pi**2 * F**3)) # convert to power spectral density
# pcov[0] *= ((3 * (67.1*3.24e-20)**2) / (4 * np.pi**2 * F**3))
print('Fit estimates:', popt)
#print('Variances:', np.diag(pcov))
stdevs = np.sqrt(np.diag(pcov))
print('Stdevs:', stdevs)
print('Covariance:', '{:.3e}'.format(pcov[0][1]))
print('SNR:', '{:.2f}'.format(SNR))


"""Plot decor (response functions for different channels, fit results, text box style)"""
responsefns=[r'$\mathcal{R}(f)_{\mathrm{X,Y,Z}}^{\mathrm{Fit}}=\frac{3}{10} \; \frac{1}{1+0.6(f/f_*)^2} 16sin^2(\frac{f}{f_*})(\frac{f}{f_*})^2$',
      r'$\mathcal{R}(f)_{\mathrm{A,E}}^{\mathrm{Fit}}=\frac{9}{20} \; \frac{1}{1+(9/16)(f/f_*)^2} 16sin^2(\frac{f}{f_*})(\frac{f}{f_*})^2$',
      r'$\mathcal{R}(f)_{\mathrm{T}}^{\mathrm{Fit}}=\frac{1}{4031} \; (\frac{f}{f_*})^6 \; \frac{1}{1+(5/16128)(f/f_*)^8}$']
params = '\n\n'.join((r'$\Omega$ = {0:.3e} $\pm$ {1:.3e}'.format(popt[0], stdevs[0]),
                    r'Covariance = {:.3e}'.format(pcov[0][1]),
                    r'$\chi^2$ = ' + str(chi) + ' with dof = ' + str(len(psd)-1),
                    r'SNR = {:.2f}'.format(SNR)))
box = dict(boxstyle='square', alpha=0.2, facecolor='white')


fig = plt.figure(figsize=(11,9))
gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])
ax0 = plt.subplot(gs[0])

"""TDI data (power spectral density using Welch method)"""
ax0.loglog(all_f, all_psd, '-', color='#0239A5', label=var + ' channel PSD')

"""Correct answers, where A = 3.55e-9, alpha=0.667"""
ax0.loglog(all_f, mod, color='#6B6B6B', linewidth=3.0, label='Expected fit')
ax0.loglog(all_f, gw_theoretical, '--', color='#C5C5C5', linewidth=2.0, label='Expected power law')

"""Fit results"""
#ax0.errorbar(f, psd, yerr=stdev, color='#CECECE', fmt='none', label=r'$\sigma$')
ax0.loglog(all_f, fit, color='#F15B0E', linewidth=2.5, label='Fit') # f for plotting only fit range, all_f for plotting over all frequencies
ax0.loglog(all_f, gw_signal, '--', color='#F5E921', linewidth=2.0, label=r'Power law: {:.3f} $\pm$ {:.3f}'.format(popt[1], stdevs[1])) # f for plotting only fit range, all_f for plotting over all frequencies

# ax0.set_xlabel("Frequency [Hz]")
ax0.set_ylabel(r"Power spectral density [Hz$^{-1}$]")
# ax0.set_ylim(-1e-60, 1e-37) # use if plotting on linear scale
ax0.set_title(r"Dataset = {0:s}, $f_* =$ {1:.1e} Hz, window size = {2:s}, f-range = [{3:.1e}, {4:.1e}]".format(dataset, F, seglength, f_low, f_high))
ax0.legend(loc='lower left')
ax0.text(0.295, 0.255, params, transform=ax0.transAxes, fontsize=9.8, verticalalignment='top', bbox=box)
# ax0.minorticks_off()
# ax0.grid(True, which='both',alpha=0.5)
# plt.setp(ax0.get_xticklabels(), visible=False)

"""Residuals plot"""
ax1 = plt.subplot(gs[1])
ax1.semilogx(all_f, residuals, '-', color='#5B5B5B', label='Normalized residuals')
ax1.set_ylim(-15, 15)
ax1.set_xlabel("Frequency [Hz]")
ax1.legend(loc='lower left')

# plt.tight_layout()
# plt.savefig("Chisquare_curvefit_T_32x1024.pdf")