#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  4 13:24:37 2021

@author: tiinamin

MCMC for recovering SGWB and noise parameters simultaneously.
"""

import numpy as np
from scipy import stats
from getdist.mcsamples import MCSamplesFromCobaya#, loadMCSamples
import getdist.plots as gdplt
from cobaya.run import run
import lisa # module containing instrument functions
import data # module used for reading data & selecting appropriate instrument functions


var = 'X' # TDI channel: X, Y, Z (Michelsons), or A, E, T (orthogonal, noise-uncorrelated channels)
F = 25 # reference frequency
dataset = 'instrnoise' # 'noiseless', 'randnoise' or 'instrnoise'
# chunk = 12623041 # number of time domain data samples, sampling interval is 5s
seglength = '16x1024' # length of each segment to be windowed
f_high = 1e-2 # high freq limit for the fit
f_low= 1e-5 # low freq limit for the fit


"""Initial values for the sampled parameters"""
Sa_init = lisa.acc # acceleration noise level
Sp_init = lisa.oms # displacement noise level
log10_Sa_init = np.log10(Sa_init) # -29.045757490560675 for tdi.py/LDC manual value
log10_Sp_init = np.log10(Sp_init) # -21.647817481888637 for tdi.py/LDC manual value
log10_A_init = np.log10(3.55e-9) # energy density amplitude
alpha_init = 2/3 # powerlaw spectral index


"""Functions to be used in likelihood"""
response = data.instrument(var, dataset)[1] # LISA response to GW's; see data.py for which one is used and lisa.py for definition
noise = data.instrument(var, dataset)[0] # noise function; see data.py for which one is used and lisa.py for definition

def powerlaw(f, A, alpha):
    """Defines the expected signal omega_gw = A * (f/F)**alpha * R(f)"""
    h = 0.671
    H = h * 100 * 3.24e-20
    pl_PSD = (f/F)**alpha * A * ((3 * H**2) / (4 * np.pi**2 * f**3)) # powerlaw conversion from energy density (omega) to PSD
    return pl_PSD * response(f) * 0.5 # division by 2 to account for one-sidedness of welch transform of data

def model(f, A, alpha, Sa, Sp):
    """Model for the fit, including noise if present in data"""
    return powerlaw(f, A, alpha) + noise(f, N_acc=Sa, N_pos=Sp)


"""Data"""    
all_f = data.freq(dataset, seglength, var) # entire frequency range [1e-6, 1e-1]
all_psd = data.psd(dataset, seglength, var) # PSD's for the entire frequency range
included = (all_f > f_low) & (all_f < f_high)
f = all_f[included] # frequency range for the fit
psd = all_psd[included] # data to be fitted to
N = data.N_windows(dataset, seglength)/2 # number of windows used in welching the t-domain data

#%% MCMC

"""Uncertainties"""
this_model = model(f, 10**log10_A_init, alpha_init, 10**log10_Sa_init, 10**log10_Sp_init)
# stdev = np.sqrt(noise(f)**2 + powerlaw(f, 10**log10_A_init, alpha_init)**2)/np.sqrt(N) # standard deviation for PSD; include powerlaw for the noiseless psd error
# sigma = np.sqrt(sum((psd - model(f, 10**log10_A_init, alpha_init))**2)/N)


"""Likelihood functions"""
# def log_G(log10_A, alpha):  # true Gaussian
#     return -0.5 * N * np.log(sigma**2) - 1/(2*sigma**2) * sum( (psd - model(f, 10**log10_A, alpha))**2 )
    
def log_Q(log10_A, alpha, log10_Sa, log10_Sp): # quadratic approx
    return -0.5 * N * np.trapz( ((psd - model(f, 10**log10_A, alpha, 10**log10_Sa, 10**log10_Sp))**2) / (this_model**2) )
    
# def log_S(log10_A, alpha): # symmetric Gaussian
#     return -0.5 * N * sum( (( psd - model(f, 10**log10_A, alpha))**2) / (psd**2) )
    
# def log_D(log10_A, alpha): # quadratic approx with added determinant term
#     return -0.5 * N * sum( ((psd - model(f, 10**log10_A, alpha))**2) / (this_model**2) ) - sum(np.log(this_model))

def log_LN(log10_A, alpha, log10_Sa, log10_Sp): # log-normal
    return -0.5 * N * np.trapz( np.log(psd / model(f, 10**log10_A, alpha, 10**log10_Sa, 10**log10_Sp))**2 )

def log_W(log10_A, alpha, log10_Sa, log10_Sp): # weighted
    return log_Q(log10_A, alpha, log10_Sa, log10_Sp)/3 + 2*log_LN(log10_A, alpha, log10_Sa, log10_Sp)/3

def log_X2(log10_A, alpha, log10_Sa, log10_Sp): # chi2
    return np.trapz(stats.chi2.logpdf(psd, df=N, loc=0, scale=model(f, 10**log10_A, alpha, 10**log10_Sa, 10**log10_Sp)/N))


"""MCMC setup"""
info = {"likelihood": {"lisa_log_like": log_X2}}

info["params"] = { # define priors for likelihood input parameters
    "log10_A": {"prior": {"min": np.log10(3e-11), "max": np.log10(3e-7)}, "ref": log10_A_init, "latex": r'\log_{10}\Omega_{\alpha}', "proposal": 0.01}, # proposal affects the width of distribution
    "alpha": {"prior": {"min": 0, "max": 1.5}, "ref": alpha_init, "latex": r'\alpha', "proposal": 0.01},
    "log10_Sa": {"prior": {"min": np.log10(0.8*Sa_init), "max": np.log10(1.2*Sa_init)}, "ref": log10_Sa_init, "latex": r'\log_{10}S_a', "proposal": 0.01}, # FIXME are these values ok?
    "log10_Sp": {"prior": {"min": np.log10(0.8*Sp_init), "max": np.log10(1.2*Sp_init)}, "ref": log10_Sp_init, "latex": r'\log_{10}S_p', "proposal": 0.01}} # FIXME are these values ok?

info["sampler"] = {"mcmc": {"Rminus1_stop": 0.001, "max_tries": 100000}} # sampler settings: 
    # Rminus1_stop = tolerance for deciding when the chains are converged, with a smaller number meaning better convergence
    # max_tries = limit on tries before chain gets stuck - could also be np.inf
    
updated_info, sampler = run(info)
gdsamples = MCSamplesFromCobaya(updated_info, sampler.products()["sample"]) # use getdist for plotting samples

#%% PLOTTING

"""Markers & legend"""
log10_A = np.log10(3.5517564350312832e-9)
alpha = 2/3
log10_Sa = np.log10(lisa.acc)
log10_Sp = np.log10(lisa.oms)
fiducial = {"log10_A": log10_A, "alpha": alpha, "log10_Sa": log10_Sa, "log10_Sp": log10_Sp} # parameter values injected in the simulation

# means = gdsamples.means # get the dist peaks if including results in legend
# log10_A_post = means[0]
# alpha_post = means[1]
# log10_Sa_post = means[2]
# log10_Sp_post = means[3]

# log10_A_bias = log10_A_post-log10_A # difference between fiducial value & result
# alpha_bias = alpha_post-alpha
# log10_Sa_bias = log10_Sa_post - log10_Sa
# log10_Sp_bias = log10_Sp_post - log10_Sp


"""Extra info"""
likestats = gdsamples.getLikeStats()
# print("Likelihood statistics:\n", likestats)
covmat = gdsamples.getCovMat().matrix[:,:5]
# print("Covariance matrix:\n", covmat)

labels = '\n\n'.join(('Channel: ' + var,
                    r'$\mathcal{L}$ = ' + info.get("likelihood").get("lisa_log_like").__name__,
                    dataset + ', w = ' + seglength,
                    'f-range: {0:.1e} - {1:.1e} Hz'.format(f_low, f_high),
                    'R = ' + response.__name__))#,
                    # r'log10_A: {0:.4f}, bias: {1:.4f}'.format(log10_A_post, log10_A_bias),
                    # r'$\alpha$: {0:.4f}, bias: {1:.4f}'.format(alpha_post, alpha_bias),
                    # r'$S_a$: {0:.4f}, bias: {1:.4f}'.format(log10_Sa_post, log10_Sa_bias),
                    # r'$S_p$: {0:.4f}, bias: {1:.4f}'.format(log10_Sp_post, log10_Sp_bias)))


"""Plotting"""                    
gdplot = gdplt.getSubplotPlotter(analysis_settings={'ignore_rows': 0.3}, width_inch=7)
gdset = gdplot.settings
gdset.constrained_layout = True
gdset.legend_frame = False
gdset.legend_colored_text = True

gdplot.triangle_plot(gdsamples, ["log10_A", "alpha", "log10_Sa", "log10_Sp"], 
                     filled=True, 
                     markers=fiducial, 
                     legend_labels=[labels], 
                     legend_loc='upper right', 
                     contour_colors=['royalblue'],
                     title_limit=1)


"""Zoom in to plots"""
# ax = gdplot.subplots[3,0] # FIXME isn't there a way to zoom in to just the contour plot?!
# ax.set_xlim(-8.7, -8.2)
# ax.set_ylim(0.62, 0.71)


# gdplot.export("SGWB_MCMC_noisefit_T_X2_32x1024.pdf")