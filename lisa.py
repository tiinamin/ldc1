#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 12:42:45 2020

@author: tiinamin
"""

import numpy as np
from scipy.fft import fft
from scipy.signal import csd, welch
from scipy.special import sici

F = 19.09e-3 # LISA transfer frequency = c/(2πL) [Hz]
L = 2.5e9 # LISA arm length [m]
c = 299792458 # speed of light [m/s]
acc = (3.0e-15)**2 # rms-amplitude of acceleration by distrubing forces at 0.1 mHz, per sensor [m^2/sec^4/Hz]
oms = (15.0e-12)**2 # single-link optical metrology noise level = rms amplitude of of optical path length fluctuation [m^2/Hz]
a_lim = 0.4e-3 # frequency limit below which accel_n rises with f^2 [Hz]
lim = 8.0e-3 # some other limit or sth [Hz]
o_lim = 2.0e-3 # frequency limit below which global_n rises with f^2 [Hz]
noise_params_SciRD = (a_lim, lim, o_lim)


def AET(X,Y,Z):
    """Derive noise-uncorrelated TDI channels from X, Y, Z."""
    A = (Z - X)/np.sqrt(2.0)
    E = (X - 2.0*Y + Z)/np.sqrt(6.0)
    T = (X + Y + Z)/np.sqrt(3.0)
    return A, E, T

def S(var, N):
    """Fourier-transform the time-domain data into PSD;
    
    Keyword arguments:
    var = TDI variable (X, Y, Z, A, E, or T)
    N = length of time-domain data
    """
    sig_fft = abs(fft(var)[0:N//2])**2 # norm='ortho', 'backward', 'forward' ; default is 'backward' = no normalization from t to f
    return sig_fft

def noises(f, N_acc=acc, N_pos=oms, params=noise_params_SciRD):
    """Return LISA acceleration and optical metrology system noises."""
    w = 2.0*np.pi*f # for converting to relative frequency unit
    S_acc = N_acc * (1.0+(params[0]/f)**2) * 1.0/(w*c)**2 * (1.0+(f/params[1])**4)
    S_oms = N_pos * (1.0+(params[2]/f)**4) * (w/c)**2
    return S_acc, S_oms

def N_XYZ(f, N_acc=acc, N_pos=oms):
    """Return noise spectrum for X, Y or Z channels."""
    w = 2.0*np.pi*f
    S_acc, S_oms = noises(f, N_acc, N_pos)
    N_XYZ = 16.0 * np.sin(w*L/c)**2 * (2.0 * (1.0+np.cos(w*L/c)**2) * S_acc + S_oms) # tdi.py definition
    # N_XYZ = 64.0 * np.sin(w*L/c)**2 * np.sin(2*w*L/c)**2 * S_oms
    # N_XYZ += 256.0 * (3 + np.cos(2*w*L/c)) * np.cos(w*L/c)**2 * np.sin(w*L/c)**4 * S_acc
    return N_XYZ

def N_AE(f, N_acc=acc, N_pos=oms):
    """Return noise spectrum for A or E channels."""
    w = 2.0*np.pi*f
    S_acc, S_oms = noises(f, N_acc, N_pos)
    #N_AE = ((4+2*np.cos(f/F))*S_oms + 8*(1+np.cos(f/F)) + np.cos(f/F)**2 * S_acc) * 4*np.sin(f/F)**2 # Smith & Caldwell definition
    N_AE = 8.0 * np.sin(w*L/c)**2 * (2.0 * S_acc * (3.0+2.0*np.cos(w*L/c)+np.cos(2*w*L/c)) + S_oms * (2.0+np.cos(w*L/c))) # tdi.py definition
    # N_AE = 8.0 * np.sin(w*L/c)**2 * (4.0 * (1.0 + np.cos(w*L/c) + np.cos(w*L/c)**2) * S_acc + (2.0 + np.cos(w*L/c)) * S_oms) # LDC manual definition (same thing as tdi.py)
    return N_AE

def N_T(f, N_acc=acc, N_pos=oms):
    """Return noise spectrum for T channel."""
    w = 2.0*np.pi*f
    S_acc, S_oms = noises(f, N_acc, N_pos)
    N_T = 16.0 * S_oms * np.sin(w*L/c)**2 * (1.0-np.cos(w*L/c)) + 128.0 * S_acc * np.sin(w*L/c)**2 * np.sin(0.5*w*L/c)**4 # tdi.py definition
    # N_T = 32.0 * np.sin(w*L/c)**2 * np.sin(0.5*w*L/c) * (4.0 * np.sin(0.5*w*L/c)**2 * S_acc + S_oms) # 
    return N_T

def N_XYcross(f, N_acc=acc, N_pos=oms):
    """Return XY noise cross-spectrum."""
    w = 2.0*np.pi*f
    S_acc, S_oms = noises(f, N_acc, N_pos)
    return -8.0 * np.sin(w*L)**2 * np.cos(w*L) * (S_oms + 4.0*S_acc) # tdi.py definition

def R_XYZ(f, datatype="doppler"):
    """Return analytical approximation of the response function for X, Y or Z,
    as presented in Caprini et al. (2019): Reconstructing the spectral shape of a SGWB with LISA."""
    R_XYZ = (3/10) * (1+0.6*(f/F)**2)**(-1) * 16*np.sin(f/F)**2 
    if datatype == "doppler":
        R_XYZ *= (f/F)**2 
    return R_XYZ

def R_AE(f):
    """Return analytical approximation for the response function for A and E,
    as presented in Smith & Caldwell (2019): LISA for Cosmologists."""
    R_AE = (9/20) * (1+(9/16)*(f/F)**2)**(-1) * 16*np.sin(f/F)**2 * (f/F)**2 #* L**2 # Smith & Caldwell # FIXME 9/16 or 0.7?
    return R_AE

def R_A(f):
    """Return analytical approximation for the response function for A,
    as presented in Boileau et al. (2020)."""
    R_AAEE = 4*np.sin(f/F)**2*(3/10 - (169/1680)*(f/F)**2 + (85/6048)*(f/F)**4 - (178273/159667200)*(f/F)**6 + (19121/24766560000)*(f/F)**8) # Adams & Cornish 2010
    R_A = R_AAEE * (16/9) * (2/np.pi) * (f/F)**4 * np.sin(f/F)**(-2) # Boileau et al.
    return R_A

def R_E(f):
    """Return analytical approximation for the response function for E,
    as presented in Boileau et al. (2020)."""
    R_AAEE = 4*np.sin(f/F)**2*(3/10 - (169/1680)*(f/F)**2 + (85/6048)*(f/F)**4 - (178273/159667200)*(f/F)**6 + (19121/24766560000)*(f/F)**8) # Adams & Cornish 2010
    R_E = R_AAEE * (16/7) * (2/np.pi) * (f/F)**4 * np.sin(f/F)**(-2) # Boileau et al.
    return R_E

def R_T(f):
    """Return analytical approximation for the response function for T,
    as presented in Smith & Caldwell (2019): LISA for Cosmologists."""
    R_T = (1/4031) * (f/F)**6 * (1+(5/16128)*(f/F)**8)**(-1) * 16*np.sin(f/F)**2 * (f/F)**2
    return R_T

def Si(x):
    si = sici(x)[0]
    return si

def Ci(x):
    ci = sici(x)[1]
    return ci

def R_XYZ_exact(f, datatype="doppler"):
    """Return an exact analytical expression for the response function for X, Y, Z.
    Note: rounding errors at low frequencies."""
    u = f/F
    gamma = np.pi/3
    uplus = u + u*np.sin(gamma/2)
    uminus= u - u*np.sin(gamma/2)
    R_exact = (1/u**2) * ( np.sin(2*u) * (np.sin(gamma/2)**2 * (1/u + 2/u**3) + np.cos(gamma/2)**2 * (2*Si(2*u) - Si(2*uplus) - Si(2*uminus)))
                    + np.cos(2*u) * (np.sin(gamma/2)**2 * (1/6 - 2/u**2) + np.cos(gamma/2)**2 * (2*Ci(2*u) - Ci(2*uplus) - Ci(2*uminus) + np.log(np.cos(gamma/2)**2)))
                    - (np.sin(uplus-uminus)/(32*u*np.sin(gamma/2)**3)) * (21 - 28*np.cos(gamma) + 7*np.cos(2*gamma) + (3-np.cos(gamma))/u**2)
                    + (np.cos(uplus-uminus)/(8*u**2*np.sin(gamma/2)**2)) * (1 + np.sin(gamma/2)**2)
                    - 2 * np.sin(gamma/2)**2 * (Ci(2*u) - Ci(uplus-uminus) + np.log(np.sin(gamma/2))) + (3-np.cos(gamma))/12 - (1-np.cos(gamma))/(u**2) )
    R_exact_16sin = R_exact * 16*np.sin(f/F)**2
    if datatype == "doppler":
        R_exact_16sin *= (f/F)**2
    return R_exact_16sin

def R_combination(f):
    """Combination of R_XYZ & R_XYZ_exact to account for rounding errors in R_XYZ_exact."""
    if isinstance(f, float):
        if f < 1e-4:
            R = R_XYZ(f)
        else:
            R = R_XYZ_exact(f)
    else:
        f_low = f[f < 1e-4]
        f = f[f >= 1e-4]
        R = np.concatenate([R_XYZ(f_low), R_XYZ_exact(f)])
    return R